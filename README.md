## Setup

In the project directory, run `yarn install` or `npm install` to install the project dependencies.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

This script will start the create-react-app and the express backend simultaneously.
Once you access the project, you will see the pivot table populated with the data from the SalesOrder xlsx worksheet.

### `yarn test`

Launches the test runner in the interactive watch mode.\
