import express from 'express';
import dataRouter from './routes/data_router';

const app = express()
const port = 4040

/**
 * I like to break my router's out early to help organize my API's
 * Probably overkill for the coding challenge, but I don't think it's a bad habit
 */
app.use('/', dataRouter);

app.listen(port);