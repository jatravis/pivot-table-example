import { WorkSheet } from "xlsx/types";

/**
 * 
 * @param worksheet The XLSX worksheet you want to format for a pivot data table
 * @returns An array of array's of strings, formatted for pivot data table
 * 
 * This should probably happen on the client? No real reason to format this on the server?
 */
const formatPivotData = (worksheet: WorkSheet) : Array<Array<string>> => {
  const attributesArray = [];

  //Need the ref here to determine the size of the table
  const ref = worksheet["!ref"]; //Ex A1-G44
  const refCells = ref.split(':');
  const beginningHeaderChar = refCells[0].charAt(0);
  const endingHeaderChar = refCells[1].charAt(0);

  //While this works for now, I think I would need to delete any possible extra params
  //Could make a helper function here to clear any non row data from the object?
  if(worksheet["!ref"]){
    delete worksheet["!ref"];
  }

  if(worksheet["!margins"]){
    delete worksheet["!margins"];
  }

  const worksheetArray = Object.entries(worksheet);
  attributesArray.push(getAttributes(worksheetArray, beginningHeaderChar));

  const worksheetValuesArray = worksheetArray.splice(attributesArray[0].length,worksheetArray.length-1);
  const valuesArray = getValues(worksheetValuesArray, endingHeaderChar);
  const returnData = attributesArray.concat(valuesArray);

  return returnData;
};

/**
 * 
 * @param worksheetArray The xlsx file represented as an array of array's of strings
 * @param beginningHeaderChar The first column that the worksheet starts in 
 * @returns An array of strings containing the attribute headers for the file
 */
function getAttributes(worksheetArray: Array<Array<string>>, beginningHeaderChar: string) : Array<string> {

  const attributeArray = [];

  //w here is the formatted text of the cell
  //Source: https://www.npmjs.com/package/xlsx
  attributeArray.push(worksheetArray[0][1]['w' as any]); //w here is the formatted text 

  //Add the first attribute
  let iterator = 1;
  let currentChar = worksheetArray[iterator][0].charAt(0);

  //Loop until we return back to the beeginning
  while(currentChar != beginningHeaderChar){
    attributeArray.push(worksheetArray[iterator][1]['w' as any])
    iterator++;
    currentChar = worksheetArray[iterator][0].charAt(0);
  }

  return attributeArray;
}

/**
 * 
 * @param worksheetValuesArray The xlsx file represented as an array of array's of strings without the headers
 * @param endingHeaderChar The last column that the worksheet uses 
 * @returns An array of array's of strings where each inner array is a row of data
 */
function getValues(worksheetValuesArray: Array<Array<string>>, endingHeaderChar: string): Array<Array<string>> {
  const valueArray = [];
  let rowArray = [];

  //Build up a new array, and push into our value array once we reach the end character
  for(let i=0; i<=worksheetValuesArray.length-1; i++){
    rowArray.push(worksheetValuesArray[i][1]['w' as any]);
    if(worksheetValuesArray[i][0].charAt(0) === endingHeaderChar){
      valueArray.push(rowArray);
      rowArray=[];
    }
  }

  return valueArray;
}

export default formatPivotData;