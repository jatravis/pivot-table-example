import XLSX from 'xlsx';

/**
 * 
 * @param filePath The filePath to the XLSX file 
 * @param opts Parsing options, currently only sets which sheets to parse
 * @returns The parsed XLSX file
 */
const loadXLSX = (filePath: string, opts?: {sheets: Array<string>}) : XLSX.WorkBook => {
  try{
    const workbook = XLSX.readFile(filePath, {sheets: opts.sheets});
    return workbook;
  } catch(e){
    throw new Error(e.message);
  }
};
export default loadXLSX;