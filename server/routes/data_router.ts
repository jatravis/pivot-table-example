import express from 'express'
const dataRouter = express.Router();

//Lib Import
import loadXLSX from '../lib/load_xlsx';
import formatPivotData from '../lib/format_pivot_data';

//Data Router
dataRouter.get('/data', (req, res) => {
    try{
        const workbook = loadXLSX('./server/data/SampleData.xlsx', {sheets: ['SalesOrders']});
        const SalesOrders = workbook['Sheets']['SalesOrders'];
        const pivotData = formatPivotData(SalesOrders);
        res.status(200).json({"pivotData": pivotData});

    } catch(err){
        res.status(500).json({error: err.message});
    }
});

//Catch all for non-existent routes
dataRouter.all('*', (req, res) => {
    res.status(404).json({value: "Route does not exist."});
  });

export default dataRouter;