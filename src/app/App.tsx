import './App.css';
import Pivot from '../pivot/Pivot';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        Pivot Table Example
      </header>
      <div>
        <Pivot/>
      </div>
    </div>
  );
}

export default App;
