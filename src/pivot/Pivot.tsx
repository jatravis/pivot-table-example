import './Pivot.css';
import PivotTableUI from 'react-pivottable/PivotTableUI';
import {getPivotData} from './services/get_pivot_data';
import { useEffect, useState } from 'react';

function Pivot() {
  const [pivotData, setPivotData] = useState([]);
  const [onChangeData, setOnChangeData] = useState([]);

  useEffect(() => {
    getPivotData()
      .then(data => {
        setPivotData(data['pivotData']);
      })
      .catch((error)=>{
        alert(error.message);
      })
  }, []);

  return (
    <div className="pvtContainer">
      <PivotTableUI
         data={pivotData}
         onChange={(s: any) => {
           setOnChangeData(s);
         }}
         {...onChangeData}
      />
    </div>
  );
}

export default Pivot;
