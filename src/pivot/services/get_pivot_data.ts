import { response } from "express";

/**
 * 
 * @returns Data for the pivot table from the /data endpoint
 */
export function getPivotData() {
    return fetch('/data')
    .then(response => {
      if(response.status !== 200){
        throw new Error("Could not retrieve data form the server");
      } else{
        return response.json();
      }

    })
    .catch((e) => {
      throw new Error("Could not retrieve data form the server");
    });
}